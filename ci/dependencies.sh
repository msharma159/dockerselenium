#!/usr/bin/env sh

set -eu
apk add build-base
apk add python-dev
apk add libffi-dev 
apk add openssl-dev
# Add python pip and bash
apk add --no-cache py-pip bash

# Install docker-compose via pip
pip install --no-cache-dir docker-compose
docker-compose -v